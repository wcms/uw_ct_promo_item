<?php

/**
 * @file
 * uw_ct_promo_item.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_promo_item_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_promotional_item content'.
  $permissions['create uw_promotional_item content'] = array(
    'name' => 'create uw_promotional_item content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_promotional_item content'.
  $permissions['delete any uw_promotional_item content'] = array(
    'name' => 'delete any uw_promotional_item content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_promotional_item content'.
  $permissions['delete own uw_promotional_item content'] = array(
    'name' => 'delete own uw_promotional_item content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_promotional_item content'.
  $permissions['edit any uw_promotional_item content'] = array(
    'name' => 'edit any uw_promotional_item content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_promotional_item content'.
  $permissions['edit own uw_promotional_item content'] = array(
    'name' => 'edit own uw_promotional_item content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_promotional_item revision log entry'.
  $permissions['enter uw_promotional_item revision log entry'] = array(
    'name' => 'enter uw_promotional_item revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_promotional_item authored by option'.
  $permissions['override uw_promotional_item authored by option'] = array(
    'name' => 'override uw_promotional_item authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_promotional_item authored on option'.
  $permissions['override uw_promotional_item authored on option'] = array(
    'name' => 'override uw_promotional_item authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_promotional_item promote to front page option'.
  $permissions['override uw_promotional_item promote to front page option'] = array(
    'name' => 'override uw_promotional_item promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_promotional_item published option'.
  $permissions['override uw_promotional_item published option'] = array(
    'name' => 'override uw_promotional_item published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_promotional_item revision option'.
  $permissions['override uw_promotional_item revision option'] = array(
    'name' => 'override uw_promotional_item revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_promotional_item sticky option'.
  $permissions['override uw_promotional_item sticky option'] = array(
    'name' => 'override uw_promotional_item sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}
