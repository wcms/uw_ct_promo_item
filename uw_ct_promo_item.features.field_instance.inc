<?php

/**
 * @file
 * uw_ct_promo_item.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uw_ct_promo_item_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-uw_promotional_item-field_block_visibility'.
  $field_instances['node-uw_promotional_item-field_block_visibility'] = array(
    'bundle' => 'uw_promotional_item',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '<p><em>Visibility</em> controls where this promotional item will appear. Leave this field blank to have this promotional item appear on all pages.</p><p>If you would like to limit the pages on which this promotional item appears, enter paths above, one per line. You may use an asterisk ("*") to include all items underneath a path. To exclude a path, start the line with "not". For example:<code><br>news<br>news/*<br>not news/special</code></p><p>To reference the home page, use the tag <code>&lt;front&gt;</code>.</p>',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'forward' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'ical' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_block_visibility',
    'label' => 'Visibility',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'exclude_cv' => FALSE,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 3,
      ),
      'type' => 'text_textarea',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-uw_promotional_item-field_body_no_summary'.
  $field_instances['node-uw_promotional_item-field_body_no_summary'] = array(
    'bundle' => 'uw_promotional_item',
    'default_value' => array(
      0 => array(
        'format' => 'uw_tf_standard_sidebar',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'forward' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'ical' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_body_no_summary',
    'label' => 'Body',
    'required' => 1,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'full_html' => 'full_html',
          'php_code' => 0,
          'plain_text' => 0,
          'uw_tf_contact' => 0,
          'uw_tf_standard' => 0,
          'uw_tf_standard_sidebar' => 'uw_tf_standard_sidebar',
          'uw_tf_standard_site_footer' => 0,
          'uw_tf_standard_wide' => 0,
        ),
        'allowed_formats_toggle' => 1,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'full_html' => array(
              'weight' => -6,
            ),
            'php_code' => array(
              'weight' => 11,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
            'uw_tf_contact' => array(
              'weight' => -5,
            ),
            'uw_tf_standard' => array(
              'weight' => -10,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -8,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -7,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -9,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'node-uw_promotional_item-field_do_not_display_title'.
  $field_instances['node-uw_promotional_item-field_do_not_display_title'] = array(
    'bundle' => 'uw_promotional_item',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'forward' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'ical' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_do_not_display_title',
    'label' => 'Do not display heading',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'exclude_cv' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-uw_promotional_item-field_file'.
  $field_instances['node-uw_promotional_item-field_file'] = array(
    'bundle' => 'uw_promotional_item',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'forward' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'ical' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'exclude_cv' => 0,
    'field_name' => 'field_file',
    'label' => 'Choose a file',
    'required' => 0,
    'settings' => array(
      'description_field' => 1,
      'entity_translation_sync' => FALSE,
      'file_directory' => 'uploads/files',
      'file_extensions' => 'bib csv doc docx gz key m mw mp3 rtf pdf pot potx pps ppt pptx psd r tar tex txt wav xls xlsx zip',
      'max_filesize' => '25 MB',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'attach' => 0,
            'clipboard' => 0,
            'imce' => 'imce',
            'reference' => 'reference',
            'remote' => 'remote',
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'copy',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 1,
          ),
          'source_reference' => array(
            'autocomplete' => 1,
            'search_all_fields' => 1,
          ),
        ),
        'insert' => 1,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'icon_link' => 0,
          'image' => 0,
          'image_banner-750w' => 0,
          'image_body-500px-wide' => 0,
          'image_galleryformatter_slide' => 0,
          'image_galleryformatter_thumb' => 0,
          'image_image_gallery_slide' => 0,
          'image_large' => 0,
          'image_medium' => 0,
          'image_person-profile-list' => 0,
          'image_profile-photo-block' => 0,
          'image_sidebar-220px-wide' => 0,
          'image_thumbnail' => 0,
          'image_wide-body-750px-wide' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-uw_promotional_item-field_image'.
  $field_instances['node-uw_promotional_item-field_image'] = array(
    'bundle' => 'uw_promotional_item',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'forward' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'ical' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_image',
    'label' => 'Choose an image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'alt_field_required' => 0,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => 'uploads/images',
      'file_extensions' => 'png gif jpg jpeg',
      'focal_point' => 0,
      'image_field_caption' => 0,
      'image_field_caption_wrapper' => array(
        'image_field_caption_default' => array(
          'format' => 'uw_tf_standard',
          'value' => '',
        ),
      ),
      'max_filesize' => '10 MB',
      'max_resolution' => '3000x3000',
      'min_resolution' => '10x10',
      'title_field' => 0,
      'title_field_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'assetbank' => 'assetbank',
            'attach' => 0,
            'clipboard' => 0,
            'imce' => 'imce',
            'reference' => 'reference',
            'remote' => 'remote',
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'copy',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 1,
          ),
          'source_reference' => array(
            'autocomplete' => 1,
            'search_all_fields' => 1,
          ),
        ),
        'insert' => 1,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'image_sidebar-220px-wide',
        'insert_styles' => array(
          'auto' => 0,
          'icon_link' => 0,
          'image' => 'image',
          'image_banner-750w' => 0,
          'image_body-500px-wide' => 0,
          'image_galleryformatter_slide' => 0,
          'image_galleryformatter_thumb' => 0,
          'image_image_gallery_slide' => 0,
          'image_large' => 0,
          'image_medium' => 0,
          'image_person-profile-list' => 0,
          'image_profile-photo-block' => 0,
          'image_sidebar-220px-wide' => 'image_sidebar-220px-wide',
          'image_thumbnail' => 0,
          'image_wide-body-750px-wide' => 0,
          'link' => 0,
        ),
        'insert_width' => 500,
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'node-uw_promotional_item-field_randomization_group'.
  $field_instances['node-uw_promotional_item-field_randomization_group'] = array(
    'bundle' => 'uw_promotional_item',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'If entered, a random promotional item from this group will be displayed.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'forward' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'ical' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_randomization_group',
    'label' => 'Randomization group',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'full_html' => 'full_html',
          'plain_text' => 'plain_text',
          'uw_tf_basic' => 'uw_tf_basic',
          'uw_tf_comment' => 'uw_tf_comment',
          'uw_tf_contact' => 'uw_tf_contact',
          'uw_tf_standard' => 'uw_tf_standard',
          'uw_tf_standard_sidebar' => 'uw_tf_standard_sidebar',
          'uw_tf_standard_site_footer' => 'uw_tf_standard_site_footer',
          'uw_tf_standard_wide' => 'uw_tf_standard_wide',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'full_html' => array(
              'weight' => -6,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
            'uw_tf_basic' => array(
              'weight' => 0,
            ),
            'uw_tf_comment' => array(
              'weight' => 0,
            ),
            'uw_tf_contact' => array(
              'weight' => -5,
            ),
            'uw_tf_standard' => array(
              'weight' => -10,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -8,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -7,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -9,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'label_help_description' => '',
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-uw_promotional_item-title_field'.
  $field_instances['node-uw_promotional_item-title_field'] = array(
    'bundle' => 'uw_promotional_item',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'title_field',
    'label' => 'Headings',
    'required' => 1,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'full_html' => 'full_html',
          'plain_text' => 'plain_text',
          'uw_tf_basic' => 'uw_tf_basic',
          'uw_tf_comment' => 'uw_tf_comment',
          'uw_tf_contact' => 'uw_tf_contact',
          'uw_tf_standard' => 'uw_tf_standard',
          'uw_tf_standard_sidebar' => 'uw_tf_standard_sidebar',
          'uw_tf_standard_site_footer' => 'uw_tf_standard_site_footer',
          'uw_tf_standard_wide' => 'uw_tf_standard_wide',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'full_html' => array(
              'weight' => -6,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
            'uw_tf_basic' => array(
              'weight' => 0,
            ),
            'uw_tf_comment' => array(
              'weight' => 0,
            ),
            'uw_tf_contact' => array(
              'weight' => -5,
            ),
            'uw_tf_standard' => array(
              'weight' => -10,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -8,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -7,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -9,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'hide_label' => array(
        'entity' => 0,
        'page' => 0,
      ),
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'label_help_description' => '',
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('<p><em>Visibility</em> controls where this promotional item will appear. Leave this field blank to have this promotional item appear on all pages.</p><p>If you would like to limit the pages on which this promotional item appears, enter paths above, one per line. You may use an asterisk ("*") to include all items underneath a path. To exclude a path, start the line with "not". For example:<code><br>news<br>news/*<br>not news/special</code></p><p>To reference the home page, use the tag <code>&lt;front&gt;</code>.</p>');
  t('Body');
  t('Choose a file');
  t('Choose an image');
  t('Do not display heading');
  t('Headings');
  t('If entered, a random promotional item from this group will be displayed.');
  t('Randomization group');
  t('Visibility');

  return $field_instances;
}
