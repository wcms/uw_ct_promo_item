<?php

/**
 * @file
 * uw_ct_promo_item.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_promo_item_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'promotional_block';
  $context->description = 'Promotional block to appear in the sidebar on every page.';
  $context->tag = 'content';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-d1d9b2f1fda78b3aa933d307dcdc2bb8' => array(
          'module' => 'views',
          'delta' => 'd1d9b2f1fda78b3aa933d307dcdc2bb8',
          'region' => 'sidebar_second',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Promotional block to appear in the sidebar on every page.');
  t('content');
  $export['promotional_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'promotional_block_sticky';
  $context->description = 'Promotional block sticky to top to appear in the sidebar on every page.';
  $context->tag = 'content';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-d9eab860f6dd91973ae0b92330cc73f1' => array(
          'module' => 'views',
          'delta' => 'd9eab860f6dd91973ae0b92330cc73f1',
          'region' => 'promo',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Promotional block sticky to top to appear in the sidebar on every page.');
  t('content');
  $export['promotional_block_sticky'] = $context;

  return $export;
}
