<?php

/**
 * @file
 * uw_ct_promo_item.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_promo_item_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_promo_item_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_promo_item_node_info() {
  $items = array(
    'uw_promotional_item' => array(
      'name' => t('Promotional item'),
      'base' => 'node_content',
      'description' => t('A promotional item, which will appear on the sidebar of every page or specified pages on your site.'),
      'has_title' => '1',
      'title_label' => t('Heading'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
