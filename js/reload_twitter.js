/**
 * @file
 */

(function ($, Drupal) {
  'use strict';
  // In order to display twitter in the promo items, we need to load twitter widget in via js.
  Drupal.behaviors.uw_ct_promo_itemloadtwitter = {
    attach: function (context) {
      $(window).bind('load', function () {
        if (typeof twttr !== "undefined") {
          twttr.widgets.load();
        }
      });
    }
  };
})(jQuery, Drupal);
