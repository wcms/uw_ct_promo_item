/**
 * @file
 */

(function ($, Drupal) {

  'use strict';
  // In order to varnish proof the random promo items, we need to load them in via js.
  // Here we will load them in on ready and the will not be behind varnish.
  Drupal.behaviors.promoItemsVarnishProofRandom = {
    attach: function (context) {
      // Check if there is a sticky promo item, hide it, then varnish-proof it.
      $('.view-display-id-promotional_block_sticky').ready(function () {
        // Ensure that the promo items js only runs once.
        $('.view-display-id-promotional_block_sticky').parent().once(function () {
          // Hide the sticky promo item, so that we do not get a flash while it loads the varnish proof item.
          $('.view-display-id-promotional_block_sticky').hide();
          // Step through each of the ajax enable views with settings and find the promo sticky.
          $(Drupal.settings.views.ajaxViews).each(function () {
            // Step through each of the keys to check for promo item and sticky.
            for (var ajax_key in this) {
              // Check if we are on a view.
              if (ajax_key.indexOf('views_dom_id') >= 0) {
                // Ensure that we are on the promo item view and we are using sticky.
                if (this[ajax_key].view_name == 'manage_promotional_items' && this[ajax_key].view_display_id == 'promotional_block_sticky') {
                  // Variable to hold data from the view.
                  var data = {};
                  // Step through of the views that are ajax enabled and setup the data variable.
                  for (var views_key in Drupal.settings.views.ajaxViews[ajax_key]) {
                    data[views_key] = Drupal.settings.views.ajaxViews[ajax_key][views_key];
                  }
                  // If we are on a correct path, setup the path properly (security needed here).
                  if (location.hash) {
                    var q = decodeURIComponent(location.hash.substr(1));
                    var o = {'f':function (v) {return unescape(v).replace(/\+/g,' ');}};
                    $.each(q.match(/^\??(.*)$/)[1].split('&'), function (i,p) {
                      p = p.split('=');
                      p[1] = o.f(p[1]);
                      data[p[0]] = data[p[0]] ? ((data[p[0]] instanceof Array) ? (data[p[0]].push(p[1]),data[p[0]]) : [data[p[0]],p[1]]) : p[1];
                    });
                  }
                  // Get the data from the actual view to put in the dom.
                  $.ajax({
                    url: Drupal.settings.views.ajax_path,
                    type: 'GET',
                    data: data,
                    success: function (response) {
                      // If we have success, replace the html with the view output.
                      // Need a second ajax call here to filter the output provided by
                      // views API from above.
                      $('.view-display-id-promotional_block_sticky').parent().html(response[2].data);
                      // Attach all the javascript again so that embeds can be processed.
                      Drupal.attachBehaviors('.view-display-id-promotional_block_sticky');
                      // Now show the sticky promo item with the varnish proof item.
                      $('.view-display-id-promotional_block_sticky').show();
                    },
                    dataType: 'json'
                  });
                }
              }
            }
          });
        });
      });
      // Check if there is a sticky promo item, hide it, then varnish-proof it.
      $('.view-display-id-promotional_block').ready(function () {
        // Check if there is a non-sticky promo item, hide it, then varnish-proof it.
        $('.view-display-id-promotional_block').parent().once(function () {
          // Hide the non-sticky promo item, so that we do not get a flash while it loads the varnish proof item.
          $('.view-display-id-promotional_block_sticky').hide();

          // Step through each of the ajax enable views with settings and find the promo non-sticky.
          $(Drupal.settings.views.ajaxViews).each(function () {
            // Step through each of the keys to check for promo item and non-sticky.
            for (var ajax_key in this) {
              // Check if we are on a view.
              if (ajax_key.indexOf('views_dom_id') >= 0) {
                // Ensure that we are on the promo item view and we are using non-sticky.
                if (this[ajax_key].view_name == 'manage_promotional_items' && this[ajax_key].view_display_id == 'promotional_block') {
                  // Variable to hold data from the view.
                  var data = {};
                  // Step through of the views that are ajax enabled and setup the data variable.
                  for (var views_key in Drupal.settings.views.ajaxViews[ajax_key]) {
                    data[views_key] = Drupal.settings.views.ajaxViews[ajax_key][views_key];
                  }
                  // If we are on a correct path, setup the path properly (security needed here).
                  if (location.hash) {
                    var q = decodeURIComponent(location.hash.substr(1));
                    var o = {'f':function (v) {return unescape(v).replace(/\+/g,' ');}};
                    $.each(q.match(/^\??(.*)$/)[1].split('&'), function (i,p) {
                      p = p.split('=');
                      p[1] = o.f(p[1]);
                      data[p[0]] = data[p[0]] ? ((data[p[0]] instanceof Array) ? (data[p[0]].push(p[1]),data[p[0]]) : [data[p[0]],p[1]]) : p[1];
                    });
                  }
                  // Get the data from the actual view to put in the dom.
                  $.ajax({
                    url: Drupal.settings.views.ajax_path,
                    type: 'GET',
                    data: data,
                    success: function (response) {
                      // If we have success, replace the html with the view output.
                      $('.view-display-id-promotional_block').parent().html(response[2].data);
                      // Attach all the javascript again so that embeds can be processed.
                      Drupal.attachBehaviors('.view-display-id-promotional_block');
                      // Now show the sticky promo item with the varnish proof item.
                      $('.view-display-id-promotional_block').show();
                    },
                    dataType: 'json'
                  });
                }
              }
            }
          });
        });
      });
    }
  };
})(jQuery, Drupal);
