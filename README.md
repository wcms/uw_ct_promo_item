Promotional items now have an additional field, "Randomization Group".

If you leave this field blank, promotional items will work as they always have
in the past.

If you add text to this field, the promotional item will be added to a
"randomization group" using the text you provided as the name of the group. When
more than one promotional item has the same randomization group name, only one
of these promotional items will be displayed on a web page. The promotional item
that will be displayed is chosen randomly.

You can mix and match promotional items with and without randomization groups.

You can create as many randomization groups as necessary. Randomization group
names are case insensitive. Please note that when creating randomization groups,
it is advised that you order your promotional items so that all items in a
single randomization group are together, as promotional items display in their
selected location regardless of whether or not they are part of a randomization
group.

Randomization groups will respect your visibility settings. If one item in a
group is set to not appear on a particular page, it will be excluded from the
group on that page automatically.
